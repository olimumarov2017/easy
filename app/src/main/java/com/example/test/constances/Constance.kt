package com.example.test.constances

object Constance {
    const val DIRECTOR = "Roman"
    const val INGINER = "Katya"
    const val PROGRAMMER = "Sam"

    const val DIRECTOR_SALARY = 3500
    const val INGINER_SALARY = 2500
    const val PROGRAMMER_SALARY = 1500

    const val DIRECTOR_PASSWORD = 4444
    const val INGINER_PASSWORD = 5555
    const val PROGRAMMER_PASSWORD = 6666
}