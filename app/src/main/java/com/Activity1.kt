package com

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity

class Activity1:AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity1)
    }
    fun onClickMain(view: View){
        val intent = Intent(this,MainActivity::class.java)
        startActivity(intent)
    }
    fun onClickClose(view: View){
        finish()
    }
}