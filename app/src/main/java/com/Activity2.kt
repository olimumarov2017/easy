package com

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity

class Activity2: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity2)
    }
    fun onClickMain(view: View){
        val intent = Intent(this,MainActivity::class.java)
        startActivity(intent)
    }
    fun onClickActivity1(view: View){
        val intent = Intent(this,Activity1::class.java)
        startActivity(intent)
    }
}